//
//  MediaPost.swift
//  petila989100
//
//  Created by iSteer on 10/8/16.
//  Copyright © 2016 Lokesh. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import CoreMedia

class MediaPost: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{

    
    var movTOMp4 = MovToMp4()
    
    
    override func viewDidLoad()
    {
       super.viewDidLoad()
       
       
      
            movTOMp4.MovToMp4(filepath,name: contentName,success:
                {   (nameOfVideo) -> Void in
                    self.contentName = nameOfVideo
                   self.movTOMp4.getVideoData(nameOfVideo,success:
                        {(videoData) -> Void in
                       
                            self.videoMp4Data = videoData
                            
                        },failure:{ (string) -> Void in
                            print(string)
                    })
                },
                failure:{ () -> Void in
            
               })
         

            
        }
}
class MovToMp4
{
    
    func MovToMp4(url:NSURL,name:NSString,success:(String) -> Void,failure:() -> Void)
    {
        let avAsset = AVURLAsset(URL: url, options: nil)
        
        let startDate = NSDate()
        
        //Create Export session
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        // exportSession = AVAssetExportSession(asset: composition, presetName: mp4Quality)
        //Creating temp path to save the converted video
        
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let myDocumentPath = NSURL(fileURLWithPath: documentsDirectory).URLByAppendingPathComponent("temp.mp4").absoluteString
        let url = NSURL(fileURLWithPath: myDocumentPath)
        
        let documentsDirectory2 = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0] as NSURL
        
        let pathPrefix = name.stringByDeletingPathExtension
        let str = pathPrefix.stringByAppendingString(".mp4");
        
        let filePath = documentsDirectory2.URLByAppendingPathComponent(str)
        deleteFile(filePath)
        
        //Check if the file already exists then remove the previous file
        if NSFileManager.defaultManager().fileExistsAtPath(myDocumentPath) {
            do {
                try NSFileManager.defaultManager().removeItemAtPath(myDocumentPath)
            }
            catch let error {
                print(error)
            }
        }
        
        exportSession!.outputURL = filePath
        exportSession!.outputFileType = AVFileTypeMPEG4
        exportSession!.shouldOptimizeForNetworkUse = true
        let start = CMTimeMakeWithSeconds(0.0, 0)
        let range = CMTimeRangeMake(start, avAsset.duration)
        exportSession!.timeRange = range
        
        exportSession!.exportAsynchronouslyWithCompletionHandler({() -> Void in
            switch exportSession!.status {
            case .Failed:
                failure()
                print("%@",exportSession?.error)
            case .Cancelled:
                failure()
                print("Export canceled")
            case .Completed:
                //Video conversion finished
                let endDate = NSDate()
                let time = endDate.timeIntervalSinceDate(startDate)
                success(str)
                
            default:
                break
            }
            
        })
    }
    
    func deleteFile(filePath:NSURL) {
        guard NSFileManager.defaultManager().fileExistsAtPath(filePath.path!) else {
            return
        }
        
        do {
            try NSFileManager.defaultManager().removeItemAtPath(filePath.path!)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    func getVideoData(name:String,success:(NSData) -> Void,failure:(NSString) -> Void)
    {
        
        
        let fileManager = NSFileManager.defaultManager()
        
        let imagePAth = (self.getDirectoryPath() as NSString).stringByAppendingPathComponent(name)
        if fileManager.fileExistsAtPath(imagePAth)
        {
            var videoData = NSData()
            // let path = NSBundle.mainBundle().pathForResource("rendered-Video", ofType: "mp4")!
            // let videodata = NSData.dataWithContentsOfMappedFile(imagePAth) as? NSData
            videoData = NSData.init(contentsOfFile: imagePAth)!
            success(videoData)
            //
            //            print(videodata)
            //self.imageView.image = UIImage(contentsOfFile: imagePAth)
        }else{
            failure("NoData")
        }
    }
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}